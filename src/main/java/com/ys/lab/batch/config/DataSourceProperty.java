package com.ys.lab.batch.config;

import com.zaxxer.hikari.HikariConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.datasource.ys-lab")
public class DataSourceProperty extends HikariConfig {
    private String jdbcUrl;
    private String driverClassName;
    private String userName;
    private String passWord;
}

