package com.ys.lab.batch.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    private final DataSourceProperty dataSourceProperty;

    @Autowired
    public DataSourceConfig(DataSourceProperty dataSourceProperty) {
        this.dataSourceProperty = dataSourceProperty;
    }

    private DataSource createDataSource(DataSourceProperty dataSourceProperty) {
        HikariDataSource hikariDataSource = (HikariDataSource) DataSourceBuilder.create()
                .driverClassName(dataSourceProperty.getDriverClassName())
                .url(dataSourceProperty.getJdbcUrl())
                .username(dataSourceProperty.getUserName())
                .password(dataSourceProperty.getPassWord())
                .build();

        hikariDataSource.setTransactionIsolation("TRANSACTION_READ_COMMITTED");

        return hikariDataSource;
    }

    @Bean
    public DataSource dataSource() {
        return createDataSource(dataSourceProperty);
    }
}
